[ -f ~/.zprofile ] && source ~/.zprofile

# disable XON/XOFF flow control (Ctrl-S/Ctrl-Q)
stty -ixon

autoload -U colors && colors

# bracketed paste
set zle_bracketed_paste
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic

# vi mode
bindkey -v

# history search
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '^r' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward
bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward
bindkey -M vicmd 'k' history-beginning-search-backward
bindkey -M vicmd 'j' history-beginning-search-forward

autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd '^e' edit-command-line

# completions
autoload -U compinit && compinit
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "${XDG_RUNTIME_DIR}/zshcache"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' menu select

alias ls='ls -h --color=auto'

# beep
unsetopt BEEP

# history
setopt extended_history
setopt inc_append_history_time
setopt hist_expire_dups_first
unsetopt hist_reduce_blanks
setopt hist_verify

# zsh hooks
autoload -Uz add-zsh-hook

# prompt
autoload -Uz vcs_info
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' formats "%f(%F{yellow}%s%f)[%F{red}%b%f%u%c] "
zstyle ':vcs_info:git:*' actionformats "(%F{yellow}%s%f)[%F{red}%b%f%u%c|%F{green}%a%f] "
zstyle ':vcs_info:git:*' stagedstr "|%F{blue}S%f"
zstyle ':vcs_info:git:*' unstagedstr "|%F{magenta}U%f"

add-zsh-hook precmd vcs_info
setopt prompt_percent
setopt prompt_subst

# only set user@host and date/time for remote ssh sessions
if [ -n "$SSH_TTY" ]; then
    PROMPT_REMOTE='%F{magenta}%n%f'
    local SHORTHOST="$(hostname -s)"
    SHORTHOST="${SHORTHOST##${USER}-}"
    if [ "$USER" != "$SHORTHOST" ]; then
        PROMPT_REMOTE="${PROMPT_REMOTE}@%F{yellow}$SHORTHOST%f"
    fi
    PROMPT_REMOTE="${PROMPT_REMOTE} [%F{blue}%D %* $(date +%Z)%f] "
else
    PROMPT_REMOTE=''
fi

PROMPT_LOCATION='%F{cyan}%~%f ${vcs_info_msg_0_}'

# FIXME: invert if in normal mode, like starship
PROMPT_CHAR='%(?.%F{green}.%F{red})%(!.#.❯)%f '

# load rc files
ZSHRCDIR=${XDG_CONFIG_HOME:-$HOME/.config}/zsh/rc.d
if [ -d "$ZSHRCDIR" ]; then
    for RCFILE in $(find -L "$ZSHRCDIR" -type f | sort); do
        source "$RCFILE"
    done
fi
unset ZSHRCDIR RCFILE

export PROMPT="${PROMPT_REMOTE}${PROMPT_LOCATION}"$'\n'"${PROMPT_CHAR}"

# assert local bin supremacy
export PATH="$HOME/.local/bin:$PATH"

# filter repeated values
typeset -U PATH
typeset -U FPATH
typeset -U MANPATH
typeset -U LD_LIBRARY_PATH
typeset -U XDG_CONFIG_DIRS
typeset -U XDG_DATA_DIRS

if type khal >/dev/null && [ -f "${XDG_CONFIG_HOME}/khal/config" ]; then
    khal list --once --format "{start-time} {title}"
    echo
fi

if type ddate >/dev/null; then
    ddate 2>/dev/null
    echo
fi

if type fortune >/dev/null; then
    fortune -a
    echo
fi
