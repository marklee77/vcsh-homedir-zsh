[ -n "$ZPROFILE_RUN" ] && return

[ -f ~/.profile ] && source ~/.profile

umask 077

# set this for now
export LANG=en_US.UTF-8

# not necessarily proper, but we use XDG even when not using X
: ${XDG_CONFIG_HOME:="${HOME}/.config"}
: ${XDG_DATA_HOME:="${HOME}/.local/share"}
export XDG_CONFIG_HOME XDG_DATA_HOME

typeset -UT XDG_CONFIG_DIRS xdg_config_dirs
: ${XDG_CONFIG_DIRS:="/etc/xdg"}
XDG_CONFIG_DIRS="${XDG_CONFIG_HOME}:${XDG_CONFIG_DIRS}"
export XDG_CONFIG_DIRS

typeset -UT XDG_DATA_DIRS xdg_data_dirs
if [ -n "$XDG_DATA_DIRS" ]; then
    XDG_DATA_DIRS="${XDG_DATA_HOME}:${XDG_DATA_DIRS}:/usr/local/share:/usr/share"
else
    XDG_DATA_DIRS="${XDG_DATA_HOME}:/usr/local/share:/usr/share"
fi
export XDG_DATA_DIRS

typeset -U path
export PATH="${HOME}/.local/bin:${PATH}"

typeset -U fpath
export FPATH="${XDG_CONFIG_HOME}/zsh/functions.d:${FPATH}"
export FPATH="${XDG_CONFIG_HOME}/zsh/completions.d:${FPATH}"

export EDITOR="vi"
export VISUAL="${EDITOR}"
export KEYTIMEOUT=1

#export HISTFILE="${HOME}/.zhistory"
export HISTSIZE=120000
export SAVEHIST=100000

# load profile files
ZSHPROFILEDIR="${XDG_CONFIG_HOME}/zsh/profile.d"
if [ -d "$ZSHPROFILEDIR" ]; then
    for RCFILE in $(find -L "$ZSHPROFILEDIR" -type f | sort); do
        source "$RCFILE"
    done
fi
unset ZSHPROFILEDIR RCFILE

export ZPROFILE_RUN=1
